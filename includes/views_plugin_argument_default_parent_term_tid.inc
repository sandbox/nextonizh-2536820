<?php

/**
 * @file
 * Definition of views_parent_terms_plugin_argument_default_parent_term_tid.
 */

/**
 * Parent Taxonomy tid default argument.
 */
class views_parent_terms_plugin_argument_default_parent_term_tid extends views_plugin_argument_default {

  /**
   * Option definition method.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['parent_term_page'] = array('default' => TRUE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Form builder method.
   */
  function options_form(&$form, &$form_state) {

    $form['parent_term_page'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Load parent tid from current term page'),
      '#default_value' => $this->options['parent_term_page'],
    );

  }

  /**
   * Method to get/set views' default arguments.
   */
  function get_argument() {
    // Load default argument from taxonomy page.
    if (!empty($this->options['parent_term_page'])) {
      if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
        $tid = arg(2);
      }
      if (arg(0) == 'admin' && arg(1) == 'structure' && is_numeric(arg(count(arg()) - 1))) {
        $tid = arg(count(arg()) - 1);
      }
      $term_parents = taxonomy_get_parents($tid);
      if (isset($term_parents)) {
        return key($term_parents);
      }
    }
  }
}
