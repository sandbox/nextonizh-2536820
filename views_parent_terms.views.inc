<?php

/**
 * @file
 * Provide views plugin for views_parent_terms.module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_parent_terms_views_plugins() {
  return array(
    'module' => 'views_parent_terms',
    'argument default' => array(
      'taxonomy_parent_term_tid' => array(
        'title' => t('Taxonomy parent term of current term from from URL'),
        'handler' => 'views_parent_terms_plugin_argument_default_parent_term_tid',
        'path' => drupal_get_path('module', 'views_parent_terms') . '/includes',
        'parent' => 'fixed',
      ),
    ),
  );
}
